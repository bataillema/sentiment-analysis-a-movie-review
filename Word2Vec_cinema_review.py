'''
This time, we use the Word2Vec method to create vectors. 
There is a library presenting neural networks already trained and 
allowing to accelerate the creation of vectors. As with the two previous 
methods, we will use a Random Forest classification to make our prediction. 
The data set is always the same.

'''


import pandas as pd
import re
from bs4 import BeautifulSoup
import numpy as np

import ntlk
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize
import gensim



# Read data from files 
train = pd.read_csv( "labeledTrainData.tsv", header=0, 
 delimiter="\t", quoting=3 )
test = pd.read_csv( "testData.tsv", header=0, delimiter="\t", quoting=3 )
unlabeled_train = pd.read_csv( "unlabeledTrainData.tsv", header=0, 
 delimiter="\t", quoting=3 )
#Stop words relies on a broader context contrary to TfIdf or n-gram models.
#Thus, removing stop_words is not an option
#So, we create a function wichs cleans reviews with an option to remove
#stop_words or not



def review_to_wordlist(review, remove_stopwords = False) : 

 	# Function to convert a document to a sequence of words,
    # optionally removing stop words.  Returns a list of words.

    # 1 . Remove HTML
    review_text = BeautifulSoup(review).get_text()

    # 2 . remove non _letters
    review_text = re.sub("^[a-zA-Z]", " ", review_text)

	# 3 . convert words to lower and split them into a list
    words = review_text.lower().split()

	# 4 . Remove stop words
    if remove_stopwords == True : 
        stops = set(stopwords.words("english")) #faster to go through a set than a list
        words = [w for w in words if w not in stops]

	# 5 . return a list of words
    return words



#Load the punkt tokenizer to tokenize paragraphs into sentences
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

def review_to_sentences(review, tokenizer, remove_stopwords = False) : 

	#a list of sentence where each sentence is a list of words
	raw_sentences = tokenizer.tokenize(review.strip())

	#2 : Loop over each sentence
	sentences = []
	for raw_sentence in raw_sentences :
		if len(raw_sentence) > 0 :
			sentences.append(review_to_wordlist(raw_sentence, remove_stopwords = False))

	return sentences


sentences = [] 
print("parsing sentences from training set")
for review in train['review'] : 
	sentences += review_to_sentences(review, tokenizer)

print("Parsing sentences for unlabeled set") 
for review in unlabeled_train['review'] : 
	sentences += review_to_sentences(review, tokenizer)

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',\
    level=logging.INFO)

from gensim.models import word2vec

num_workers = 4
num_features = 300
min_word_count = 40 #Ignores all words with total frequency lower than this.
context = 10 #Maximum distance between the current and predicted word within a sentence.
downsampling = 1e-3 #The threshold for configuring which higher-frequency words are randomly downsampled, useful range is (0, 1e-5).

print('Training model ...')
model = word2vec.Word2Vec(sentences, size=num_features, workers=num_workers, min_count = min_word_count,window = context, sample = downsampling)

model.init_sims(replace = True) #to save memory, but read_only, the model can't be training 

#save the model for later use
model_name  = "300features_40minwords_10context"
model.save(model_name)

#Exploring the model result
#try some example

model.doesnt_match("berlin paris france rome".split())
model.doesnt_match("man woman child kitchen".split())
model.doesnt_match("paris berlin london austria".split())
model.doesnt_match("paris berlin london soccer".split())

#Obtain words similar to "man"
model.most_similar("man")
model.most_similar("running")
model.most_similar("cat")

#load the model and check the size
from gensim.models import Word2Vec
Word2Vec.load("300features_40minwords_10context")
type(model.syn0)
model.syn0.shape


#see the feature vector
model['flower']

#Now, our objective is finding a feature vector for each review
#thanks to word feature

#One of the idea is to find the average vector for each review
def makeFeatureVec(words, model, num_features) :

	featureVec = np.zeros((num_features,), dtype = "float32")

	nwords = 0.

	#model.index2word to obtain a list that contains words names

	index2word_set = set(model.wv.index2word)

	for word in words : 
		if word in index2word_set :
			nwords += 1
			featureVec = np.add(featureVec, model[word])

	#divide by the number of words in the model
	featureVec = np.divide(featureVec, nwords)

	return featureVec

def getAvgFeatureVecs(reviews, model, num_features) : 

	reviewFeatureVecs = np.zeros((len(reviews), num_features))

	counter = 0

	for review in reviews : 
		reviewFeatureVecs[counter] = makeFeatureVec(review, model, num_features)

		counter += 1

	return reviewFeatureVecs


#So, let's call these functions

clean_train_reviews = []

for review in train["review"] : 
	clean_train_reviews.append( review_to_wordlist(review, remove_stopwords = True))

trainDataVecs = getAvgFeatureVecs(clean_train_reviews, model, num_features)

clean_test_reviews = []

for review in test['review'] :
	clean_test_reviews.append( review_to_wordlist(review, remove_stopwords = True))

testDataVecs = getAvgFeatureVecs(clean_test_reviews, model, num_features)

#Now we use Random Forest to do a classification

from sklearn.ensemble import RandomForestClassifier

forest = RandomForestClassifier (n_estimators = 100)

print("fitting a random forest classifier of 100 trees with the training data")
y_train = train['sentiment']
forest.fit(trainDataVecs, y_train)

pred = forest.predict(testDataVecs)

output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )
output.to_csv( "Word2Vec_AverageVectors.csv", index=False, quoting=3 )

#Now, word clustering. Word2vec creates clusters of sementically related words

from sklearn import KMeans
#Traditionnally, clusters of words contains five words
word_vectors = model.syn0
num_clusters = word_vectors.shape[0] / 5 
clustering = KMeans(n_clusters = num_clusters, random_state = 42).fit_predict(word_vectors)

#Let's zip class and  words of our vocabulary
word_centroid_map = dict(zip( model.index2word, clustering))

#We can create bag of centroids and use them in a classifier
def create_bag_of_centroids(wordlist, word_centroid_map) :

	n_centroids = max(word_centroid_map.values) + 1
	bag_of_centroids = np.zeros((n_centroids))

	for word in wordlist :
		if word in word_centroid_map :
			index = word_centroid_map[word]
			bag_of_centroids[index] += 1

	return create_bag_of_centroids

#create vectors of features for each review train['review']
train_centroids = np.zeros((train['review'].size, num_clusters))

counter = 0
for review in clean_train_reviews:
	train_centroids[counter] = create_bag_of_centroids(review, word_centroid_map)

#create vectors of features for each review test['review']
test_centroids = np.zeros(( test["review"].size, num_clusters), dtype="float32" )

counter = 0
for review in clean_test_reviews:
	test_centroids[counter] = create_bag_of_centroids(review, word_centroid_map)


forest = RandomForestClassifier(n_estimators = 100)
forest.fit(train_centroids, train['sentiment'])
y_pred = forest.predict(test_centroids)

output = pd.DataFrame(data={"id":test["id"], "sentiment":y_pred})
output.to_csv( "BagOfCentroids.csv", index=False, quoting=3 )

