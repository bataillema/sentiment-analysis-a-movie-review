import re
import numpy as np
import pandas as pd

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from nltk.tokenize import RegexpTokenizer



from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score

# %% [code]
train = pd.read_csv("/kaggle/input/word2vec-nlp-tutorial/labeledTrainData.tsv", header=0, \
                    delimiter="\t", quoting=3)

# %% [code]
x_train = train['review']
y_train = train['sentiment']

# %% [code]
X_train, X_test, y_train, y_test = train_test_split (x_train, y_train, test_size=0.33, random_state=42)

# %% [markdown]
# CountVercitorizer is very fast. A lot of steps of text cleaning are done in the same time.

# %% [code]
token = RegexpTokenizer(r'[a-zA-Z0-9]+')
cv = CountVectorizer(lowercase=True,stop_words='english',ngram_range = (1,1),tokenizer = token.tokenize)
X_train = cv.fit_transform(X_train)

# %% [code]
X_train = text_counts

# %% [code]
clf = RandomForestClassifier(n_estimators=100, max_depth=8,random_state=0)
clf.fit(X_train, y_train)

# %% [code]
X_test = cv.transform(X_test)
y_pred = clf.predict(X_test)

# %% [code]
accu_score = accuracy_score(y_pred, y_test)

# %% [markdown]
# 1. The score is slightly better with BOW than with TfIdf. + 0.02 with 20 trees, 0.72 vs 0.74
# 1. The score is widely better with BOW and 100 trees !!

# %% [code]
