# %% [code]
# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list all files under the input directory

import os
for dirname, _, filenames in os.walk('/kaggle/input'):
    for filename in filenames:
        print(os.path.join(dirname, filename))

# Any results you write to the current directory are saved as output.

# %% [code]
import re
import nltk
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.probability import FreqDist

from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix

# %% [markdown]
# Import data

# %% [code]
train = pd.read_csv("/kaggle/input/word2vec-nlp-tutorial/labeledTrainData.tsv", header=0, \
                    delimiter="\t", quoting=3)

# %% [code]
x_train = train['review']

# %% [code]
y_train = train['sentiment']

# %% [markdown]
# Cleaning review !
# 1. remove any character that is not a letter
# 2. remove stop words

# %% [code]
def clean_review(texte) :
    texte = re.sub('[^a-zA-Z]', ' ', texte) #removing any character that is not a letter
    words = texte.split( ) #splitting texte into single words
    stopWords = set(stopwords.words('english')) #loading stop words in a set. Faster to run through in a set than in a list
    for word in words : #removing stop words from words
        if word in stopWords :
            words.remove(word)
        word.lower()
    
    clean_text = [' '.join(word) for word in words]
    
    return texte

# %% [code]
for i in range(x_train.shape[0]) :
    x_train[i] = clean_review(x_train[i])

# %% [markdown]
# Extracting features !!
# 
# 1. Let's try Tfidf vectorizer to transform each review in a vector
# 1. 1. Ignoring terms having a document frequency lower than "min_df"
# 1. 1. Selecting only the top 5000 words among all the documents

# %% [code]
X_train, X_test, y_train, y_test = train_test_split(x_train, y_train, test_size=0.33, random_state=42) #Splitting train and test

# %% [code]
vectorizer = TfidfVectorizer(min_df = 0.1, max_features = 5000)
X_train = vectorizer.fit_transform(X_train)

# %% [code]
clf = RandomForestClassifier(n_estimators=20, max_depth=8,random_state=0)
clf.fit(X_train, y_train)

# %% [code]
X_test = vectorizer.transform(X_test)
y_pred = clf.predict(X_test)

# %% [markdown]
# Scoring. We use the metric accuracy score

# %% [code]
y_test

# %% [code]
accuracy = accuracy_score(y_test, y_pred)
print("accuracy :", accuracy)

# %% [markdown]
# Confusion Matrix

# %% [code]
conf_matrix = confusion_matrix(y_test, y_pred)

print(conf_matrix)
#Still a lot of errors

# %% [markdown]
# Interesting for you if you consider Kaggle competition.
# Prediction for Kaggle based on the testData.csv  .

# %% [code]
test = pd.read_csv("/kaggle/input/word2vec-nlp-tutorial/testData.tsv", header=0, \
                    delimiter="\t", quoting=3)

x_test = test['review']

# %% [code]
for i in range(x_test.shape[0]) :
    x_test[i] = clean_review(x_test[i])

# %% [code]
X_test = vectorizer.transform(x_test)
y_pred = clf.predict(X_test)

# %% [code]
id_ = []
for id in test['id'] : #removing "" in test['id']
    id_.append(re.sub('"', '', id))

    # %% [code]
data = {'id' : id_, 'sentiment' : y_pred}

# %% [code]
submission = pd.DataFrame(data = data)

# %% [code]
submission.to_csv('submission.csv',index=False)